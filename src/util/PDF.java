/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 *
 * @author madar
 */
public class PDF {

    private String ruta;
    private String nombrePdf;

    public PDF() {
    }

    public PDF(String ruta, String nombrePdf) {
        this.ruta = ruta;
        this.nombrePdf = nombrePdf;
    }

    /**
     * Crea desde una cadena la tabla necesaria para crear una tabla La cadena
     * contiene : dato1 \t dato2 .... \n los separadores de cada columna es \t y
     * de cada línea \n
     *
     * @param datos_tabla
     */
    public void crear_Tabla(String datos_tabla, String miNombre) throws FileNotFoundException, DocumentException {
        //1. Crear un documento PDF
        Document documentoPDF = new Document();
        //2. Crear la referencia al archivo:
        FileOutputStream ficheroEnDisco = new FileOutputStream(this.ruta + "/" + this.nombrePdf);
        //3. Asociar la información del documento al archivo físico
        PdfWriter.getInstance(documentoPDF, ficheroEnDisco);
        //4. Abro el documento
        documentoPDF.open();
        //5. Escribir sobre el documento:
        Paragraph parrafo = new Paragraph();
        parrafo.add("Estos son mis experimentos " + miNombre + " \n \n");
        parrafo.setAlignment(Element.ALIGN_CENTER);
        String v[] = datos_tabla.split("\n");
        PdfPTable table = new PdfPTable(3);

        table.addCell("Escenario");
        table.addCell("Tamaño de matriz");
        table.addCell("Tiempo mseg");

        for (String elemento : v) {
            String datos_fila[] = elemento.split("\t");
            table.addCell(datos_fila[0]);
            table.addCell(datos_fila[1] + "x" + datos_fila[1]);
            table.addCell(datos_fila[2]);
        }

        //6. Adicionar el párrafo y la tabla al documento:
        documentoPDF.add(parrafo);
        documentoPDF.add(table);

        //7. Cerrar el archivo creado
        documentoPDF.close();

    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

}
